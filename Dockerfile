# Use the Windows Server Core as the base image
# ROM microsoft/windowsservercore:latest
FROM docker.io/microsoft/windowsservercore:latest

# Copy the PowerShell script to the container
COPY ConfigureRemotingForAnsible.ps1 C:\remoting.ps1

# Run the PowerShell script to configure remoting, set the administrator password, and enable the administrator account
RUN powershell.exe -Command \
    powershell.exe -ExecutionPolicy ByPass -File c:\remoting.ps1 -EnableCredSSP; \
    $password = ConvertTo-SecureString "Supersecure" -AsPlainText -Force; \
    Set-LocalUser -Name administrator -Password $password; \
    Enable-LocalUser -Name "Administrator"; \
    Dism.exe /online /Cleanup-Image /StartComponentCleanup /ResetBase
EXPOSE 5986
