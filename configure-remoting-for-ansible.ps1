# Enable WinRM
Enable-PSRemoting -Force

# Enable CredSSP authentication for WinRM
Enable-WSManCredSSP -Role Server -Force

# Set the WinRM service to start automatically
Set-Service -Name 'winrm' -StartupType 'Automatic'

# Allow incoming WinRM traffic on the default port (5985)
netsh advfirewall firewall add rule name="WinRM-HTTP" dir=in action=allow protocol=TCP localport=5985

# Allow incoming WinRM traffic on the secure port (5986)
netsh advfirewall firewall add rule name="WinRM-HTTPS" dir=in action=allow protocol=TCP localport=5986

# Disable the requirement for HTTPS (Note: This is not recommended for production)
Set-Item -Path "WSMan:\localhost\Service\AllowUnencrypted" -Value 1

# Restart the WinRM service to apply changes
Restart-Service -Name 'winrm'
